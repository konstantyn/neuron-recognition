from skimage.util import random_noise
from skimage import io
import numpy as np

for n in range(10):
    img = io.imread('img/png-10x12/%s.png' % n)
    for a in np.arange(0.05,0.41,0.05):
        for i in range(20):
            noised = random_noise(img, mode='s&p', amount=a)
            io.imsave('img/png-10x12/%s-%s-%s.png' % (n, a, i), noised)
