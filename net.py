from datetime import datetime
import numpy as np
import neurolab as nl
from train import getTrainingDataSet, sim_test, getTrainingDataSet10

startTime = datetime.now()

t = getTrainingDataSet10()

num_of_samples = len(t["xr"])
num_of_repeat = 1
num_of_input = len(t["xr"][0])
r = [[0,1]] * num_of_input
layers = [120,240,10]

print "num of input: %s" % num_of_input
print "layers: %s" % layers
print "samples: %s" % num_of_samples
print "repeats: %s" % num_of_repeat

net = nl.net.newff(r, layers, transf=[nl.trans.PureLin(),nl.trans.PureLin(), nl.trans.PureLin()])

nl.train.train_cg(net, t["xr"]*num_of_repeat, t["yr"]*num_of_repeat, show=15, rr=0.2)
# nl.train.train_gd(net, t["xr"]*num_of_repeat, t["yr"]*num_of_repeat, show=15, rr=0.1, epochs=3000)
sim_test(net, t)

net.save("test.net")

print datetime.now() - startTime
