from datetime import datetime
import neurolab as nl
import matplotlib.pyplot as plt

from random import random
from PIL import Image

startTime = datetime.now()
net = nl.load("test.net")

x, y = (10, 12)

num_of_input = x*y

def getTestData(i, n):
    xr = []
    for j in range(10,20):
        img = Image.open("img/png-%sx%s/%s-%s-%s.png" % (x, y, i, n, j), mode='r')
        data = list(img.getdata())[:num_of_input]
        xr.append(data)
    return xr

def responses_to_num(sim):
    return [round(i,1)*10 for i in sim]

# probability of correct classification
# Params:
#   i - number
#   n - amount of noise
def probability_noise(i, n):
    xr = getTestData(i, n)
    yr = [i for j in range(len(xr))]
    sim = net.sim(xr)
    r = responses_to_num(sim)
    num_of_recognitions = len(xr)
    num_of_correct = r.count(i)
    p = num_of_correct / float(num_of_recognitions)
    return p

# dummy test
# def probability_noise(i, n):
#     return 1-5*n**3-random()/10

def testNoise():
    layers = [l.cn for l in net.layers]
    ls = '_'.join([str(x) for x in layers])
    classes = range(10)
    m = ['b-', 'b--', 'b-.', 'r-', 'r--', 'r-.', 'g-', 'g--', 'g-.', 'b:']
    x = [0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40]
    y = [[probability_noise(i, n) for n in x] for i in classes]

    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.73, 0.8])
    ax.set_xlabel('Amount of noise')
    ax.set_ylabel('Probability of correct classification')
    ax.set_title('Quality / Noise %s' % (layers))
    for i in classes:
        ax.plot(x, y[i], m[i], label=str(i))
    ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.savefig("noise_%s.png" % ls)

testNoise()

print datetime.now() - startTime
