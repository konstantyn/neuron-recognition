from PIL import Image
import numpy as np

def getTrainingData(x,y,num_of_samples,num_of_input):
    xr = []
    for n in range(num_of_samples):
        img = Image.open("img/png-%sx%s/%s.png" % (x, y, n), mode='r')
        data = list(img.getdata())[:num_of_input]
        xr.append(data)
    return xr

def one_or_zero(x):
    return 1 if x != 0 else x

def binnarize(img):
    return map(one_or_zero, img)

def getTrainingDataSet():
    t = {
        "xr": [],
        "yr": []
    }
    for n in range(10):
        img = Image.open("img/png-10x12/%s.png" % (n), mode='r')
        data = list(img.getdata())
        t["xr"].append(data)
        t["yr"].append([n/10.0])
    for p in [0.05, 0.10, 0.15, 0.20]:
        for j in range(10):
            for n in range(10):
                img = Image.open("img/png-10x12/%s-%s-%s.png" % (n, p, j), mode='r')
                data = list(img.getdata())
                t["xr"].append(data)
                t["yr"].append([n/10.0])
    for n in range(10):
        img = Image.open("img/png-10x12/%s.png" % (n), mode='r')
        data = list(img.getdata())
        t["xr"].append(data)
        t["yr"].append([n/10.0])
    return t

def getOutputVector(n, l=10):
    y = [0 for i in range(l)]
    y[n] = 1;
    return y

def vectorToClassNum(a):
    narr = np.array(a)
    return np.where(narr == narr.max())[0][0]

def addSampleToTraining(path, n, t):
    img = Image.open(path, mode='r')
    data = list(img.getdata())
    data = binnarize(data)
    t["xr"].append(data)
    y = getOutputVector(n)
    t["yr"].append(y)

def getTrainingDataSet10():
    t = {
        "xr": [],
        "yr": []
    }
    for n in range(10):
        addSampleToTraining("img/png-10x12/%s.png" % (n), n, t)
    for p in [0.05, 0.1]:
        for j in range(10):
            for n in range(10):
                addSampleToTraining("img/png-10x12/%s-%s-%s.png" % (n, p, j), n, t)
    return t

def sim_test(net, t):
    sim = net.sim(t["xr"])
    correct = 0
    for idx, val in enumerate(sim):
        recognitedClass = vectorToClassNum(val)
        testClass = vectorToClassNum(t["yr"][idx])
        if (recognitedClass == testClass):
            correct=correct+1

    print "%s / %s" % (correct, len(t["xr"]))
