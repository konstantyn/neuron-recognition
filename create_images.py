from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
import os

def create_sign(s, x, y, font_size, offsetx, offsety):
    fonts_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fonts')
    font = ImageFont.truetype(os.path.join(fonts_path, 'arial.ttf'), font_size)
    img = Image.new("1", (x, y), 1)
    draw = ImageDraw.Draw(img)
    draw.text((offsetx, offsety), s, 0, font=font)
    draw = ImageDraw.Draw(img)
    img.save( "img/png-%sx%s/%s.png" % (x, y, s) )

def create_images(x, y, font_size, offsetx, offsety):
    directory = "img/png-%sx%s" % (x, y)
    if not os.path.exists(directory):a
        os.makedirs(directory)
    for i in range(0,10):
        create_sign(str(i), x, y, font_size, offsetx, offsety)

create_images(18, 25, 32, 0, -5)
create_images(10, 12, 16, 0, -3)
